# Automaty skończone dla różnych klas związków chemicznych



## Cel projektu

Celem projektu było zaprojektowanie odpowiednich automatów skończonych dla wybranych klas związków chemicznych - aldehydów oraz związków bicyklicznych. Zadanie zostało zrealizowane za pomocą języka programowania Python.

## Studia literaturowe

Automatem skończonym nazywa się abstrakcyjny, iteracyjny model matematyczny w teorii automatów. Niezwykle ważnym elementem jest fakt opisu przejść między poszczególnymi stanami, których liczba jest skończona.

Głównym założeniem teorii automatów są kocepty takie jak alfabet, ciąg znaków oraz język. Alfabet można opisać jako skończony, niepusty zbiór symboli. Jak sama nazwa wskazuje, ciąg znaków jest skończoną sekwencją syboli należących do danego alfabetu. Język natomiast składa się ze zbioru ciągów znaków utworzonych z symboli alfabetu.

W przypadku chemii automaty skończone można stosować do celów takich, jak sprawdzanie czy dany związek chemiczny należy do odpowiedniej grupy związków albo czy jest produkcjem odpowiedniej reakcji chemicznej.

Dla związków chemicznych również można utworzyć alfabet, z którego będą tworzone ciągi znaków. Do tego celu można stosować notację SMILES (ang. Simplified molecular-input line-entry system). Na przykładzie p - benzochinonu (C1=CC(=O)C=CC1=O) można wyciągnąć pewne wnioski - C1=CC(=O)C=CC1=O jest ciągiem znaków, na podstawie którego można wyodrębnić potencjalne symbole należące do alfabetu. Po wyodrębnieniu następujących symboli, możliwe jest zaprojektowanie automatu skończonego, który będzie akceptować p - benzochinon. Możliwe jest tworzenie automatów o szerszym zastosowaniu - np. akceptujących aldehydy alifatyczne lub bicykliczne węglowodory.

## Wykonanie

Wykonane zostały projekty dwóch automatów. Pierwszy z nich akceptuje kody SMILES prostych aldehydów alifatycznych, np. CCCC=O (butanal), CCCCC=O (pentanal). Kolejny wykazuje większy stopień skomplikowania - automat akceptujący związki bicykliczne o określonej strukturze kodu SMILES. Zasady są następujące:

- związek chemiczny jest bicykliczny, co oznacza że może w swojej konfiguracji zawierać tylko dwa pierścienie
- musi być to z wiązek z grup węglowodorów, tj. zawierać w swojej budowie wyłącznie węgiel i wodór
- kod SMILES musi mieć odpowiednią konstrukcję - powinny w nim występować atomy węgla C1 oraz C2, które określają w którym miejscu łączy się pierścień; dodatkowo SMILES musi mieć odpowiedni układ, np. C1C2C2C1 zostanie zaakceptowany. Kod musi być otwarty i zamknięty atomem C1.

## Wykonane testy

Do sprawdzenia poprawności działania kodu zastosowano kilka testów. W przypadku automatu który miał za zadanie akceptację kodów SMILES aldehydów alifatycznych przetestowano następujące przypadki:
- proste aldehydy alifatyczne takie jak uprzednio wymieniony butanal (CCCC=O), pentanal (CCCCC=O) czy oktanal (CCCCCCCC=O) o dłuższym łańcuchu sprawiały, że kod zwracał wartość True, potwierdzając prawidłowe działanie.
- aldehydy aromatyczne nie są akceptowane przez automat - kod zwraca wartość False, co jest spodziewane i pożądane. Przykładem jest benzaldehyd o kodzie SMILES C1=CC=C(C=C1)C=O oraz 2-metylobenzaldehyd o kodzie SMILES CC1=CC=CC=C1C=O.

W przypadku związków bicyklicznych przeprowadzone zostały następujące testy:
- automat powinien przyjmować kody SMILES o określonej strukturze, np. dekalina zawiera dwa pierścienie oraz jej kod SMILES ma odpowiednią budowę C1CCC2CCCCC2C1, co powoduje że zwracana jest wartość True. Bicyklobutan z kolei zawiera dwa pierścienie, ale jego kanoniczny SMILES nie spełnia wymagów automatu (C1C2C1C2), co rezultuje w zwracaniu wartości False. Benzo[a]piren jest cząsteczką policykliczną, więc tak jak się spodziewano jego smiles (c1ccc2c(c1)cc3ccc4cccc5c4c3c2cc5) nie był akceptowany przez automat.

## Wnioski

Proste związki organiczne mogą być opisywane przez skończone automaty, natomiast wraz ze stopniem skomplikowania układu potrzebne są rozwiązania o większym stopniu złożoności, takie jak automat ze stosem lub wieloma stosami, a nawet tokenizacja.

## Dodatkowe źródła
- https://match.pmf.kg.ac.rs/electronic_versions/Match82/n3/match82n3_549-560.pdf

