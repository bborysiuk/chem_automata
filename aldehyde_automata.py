def aldehyde_automata(smiles):

    iterator = 0
    state = 0

    while iterator < len(smiles):
        element = smiles[iterator]

        if state == 0:

            if element == 'C':
                state = 1
            else:
                return False

        elif state == 1:

            if element == 'C':
                state = 1
            elif element == '=':
                state = 2
            else:
                return False

        elif state == 2:

            if element == 'O':
                if iterator == len(smiles) - 1:
                    return True
                else:
                    return False
            else:
                return False

        iterator += 1

    return False


input_smiles = input("Provide SMILES: ").strip().upper()
result = aldehyde_automata(input_smiles)
print(f"{input_smiles}: {result}")
