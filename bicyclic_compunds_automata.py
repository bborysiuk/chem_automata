def automata(smiles):
    iterator = 0
    state = 0
    #stack = []

    while iterator < len(smiles):
        element = smiles[iterator]

        if state == 0:
            if element == 'C':
                state = 1
            else:
                return False

        elif state == 1:
            if element == 'C':
                state = 1
            elif element == '1':
                state = 2
            else:
                return False

        elif state == 2:
            if element == 'C':
                state = 3
            else:
                return False

        elif state == 3:
            if element == 'C':
                state = 3
            elif element == '2':
                state = 4
            else:
                return False

        elif state == 4:
            if element == 'C':
                state = 5
            else:
                return False

        elif state == 5:
            if element == 'C':
                state = 5
            elif element == '2':
                state = 6
            else:
                return False

        elif state == 6:
            if element == 'C':
                state = 7
            else:
                return False

        elif state == 7:
            if element == 'C':
                state = 7
            elif element == '1':
                if iterator == len(smiles) - 1:
                    return True
                else:
                    return False
            else:
                return False

        iterator += 1

    return False

input_smiles = input("Provide SMILES: ").strip().upper()
result = automata(input_smiles)
print(f"{input_smiles}: {result}")

